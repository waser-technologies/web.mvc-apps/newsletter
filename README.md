# Newsletter

This Web.mvc App allows you to have newsletters on your website.

# Installation

If you use [Web.mvc Cloud](https://waser.tech/fr-fr/technologies/web-mvc) you can install this App inside the Web.mvc Apps panel of your website Console.

If you use the source version of Web.mvc, install this app using pip.

`pip install git+https://gitlab.waser.tech/web.mvc-apps/newsletter.git`

Dont forget to add it to the installed apps list inside the settings file.

```
INSTALLED_APPS = [
    ...
    "newsletter",
    ]
```

Once done, migrate your database.

`python manage.py migrate newsletter`

# Thanks
This Web.mvc App is based on top of [django-newsletter](https://github.com/dokterbob/django-newsletter).