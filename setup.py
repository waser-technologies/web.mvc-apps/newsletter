import warnings

from setuptools import setup, find_packages

try:
    README = open('README.md').read()
except:
    warnings.warn('Could not read README.md')
    README = None

try:
    REQUIREMENTS = open('requirements.txt').read()
except:
    warnings.warn('Could not read requirements.txt')
    REQUIREMENTS = None

try:
    TEST_REQUIREMENTS = open('requirements_test.txt').read()
except:
    warnings.warn('Could not read requirements_test.txt')
    TEST_REQUIREMENTS = None

setup(
    name='newsletter',
    version="1.0.0",
    description=(
        'Web.mvc app for managing multiple mass-mailing lists with both '
        'plaintext as well as HTML templates (and pluggable WYSIWYG editors '
        'for messages), images and a smart queueing system all right from '
        'the admin interface.'
    ),
    long_description=README,
    install_requires=REQUIREMENTS,
    author='Danny Waser',
    author_email='danny@waser.tech',
    url='https://gitlab.waser.tech/web.mvc-apps/newsletter',
    packages=find_packages(exclude=("tests", "test_project")),
    include_package_data=True,
    classifiers=[
        'Development Status :: 6 - Mature',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Affero General Public License v3',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Topic :: Utilities'
    ],
    test_suite='runtests.run_tests',
    tests_require=TEST_REQUIREMENTS
)
