import os.path

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import ugettext_lazy as _

from .cms_plugins_models import NewsletterPluginModel
from .forms import SubscribeRequestForm

@plugin_pool.register_plugin
class NewsletterPlugin(CMSPluginBase):
    model = NewsletterPluginModel
    name = _("Newsletter")
    base_render_template = "CMSPlugin.html"
    cache = False
    text_enabled = True

    def render(self, context, instance, placeholder):
        context = super(NewsletterPlugin, self).render(context, instance, placeholder)
        context.update({'form':SubscribeRequestForm(newsletter=instance.newsletter)})
        return context
    
    def get_render_template(self, context, instance, placeholder):
        return os.path.join('newsletter', instance.template_folder, self.base_render_template)