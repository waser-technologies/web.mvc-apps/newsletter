from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import ugettext_lazy as _

from django.db import models

from .models import Newsletter
from .settings import get_setting

NEWSLETTER_PLUGIN_TEMPLATE_FOLDERS = get_setting('PLUGIN_TEMPLATE_FOLDERS')

class NewsletterPluginModel(CMSPlugin):
    title = models.CharField(_('title'), max_length=100, blank=True)
    newsletter = models.ForeignKey(Newsletter, related_name="newsletter_plugin", db_index=False, null=True, on_delete=models.SET_NULL)
    template_folder = models.CharField(
        max_length=200,
        verbose_name=_('Plugin template'),
        help_text=_('Select plugin template to load for this instance'),
        default=NEWSLETTER_PLUGIN_TEMPLATE_FOLDERS[0][0],
        choices=NEWSLETTER_PLUGIN_TEMPLATE_FOLDERS
    )
    
    def copy_relations(self, oldinstance):
        #self.newsletter.delete()
        self.newsletter = oldinstance.newsletter
        #self.newsletter.pk = None
        self.newsletter.save()
