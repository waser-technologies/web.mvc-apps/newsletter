import os
import sys

def make_migrations():
    test_project_dir = os.path.join(os.path.dirname(__file__), 'test_project')
    sys.path.insert(0, test_project_dir)

    os.environ['DJANGO_SETTINGS_MODULE'] = 'test_project.settings'
    
    from django.core.management import execute_from_command_line

    args = sys.argv + ["makemigrations", "newsletter"]
    execute_from_command_line(args)

if __name__ == "__main__":
    make_migrations()